<?php
// DIRETÓRIO DO SISTEMA
define("BASEPATH", dirname(__FILE__)."/");
define("BASEURL", "http://localhost/cursos/paineladm/");
define("ADMURL", BASEURL."painel.php");
define("CLASSESPATH", "classes/");
define("MODULOSPATH", "modulos/");
define("CSSPATH", "css/");
define("JSPATH", "js/");

// BANCO DE DADOS
define("DBHOST", "localhost");
define("DBUSER", "root");
define("DBPASS", "");
define("DBNAME", "aulas");

