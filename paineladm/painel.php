<?php 
include 'header.php'; 
if(isset($_GET['m'])) $modulo = $_GET['m'];
if(isset($_GET['t'])) $tela = $_GET['t'];
?>
<div id="content">
    <?php
    if($modulo && $tela):
        loadModulo($modulo, $tela);
    else:
        echo '<p>Escolha uma opção do menu ao lado.</p>';
    endif;
    ?>
</div><!-- content -->
<?php include 'siderbar.php'; ?>

<?php include 'footer.php'; ?>