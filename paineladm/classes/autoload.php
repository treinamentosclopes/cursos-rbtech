<?php
$pathlocal = dirname(__FILE__);
require_once dirname($pathlocal)."/funcoes.php";
function __autoload($class){
    $classe = str_replace('..', '', $class);
    require_once $pathlocal."/$classe.class.php";
}