<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">
        <title>Exemplo 2</title>
    </head>
    <body>
    	<header>
    	   <h1>Exemplo 2</h1>
    	</header>
    	<nav>
    	   <p>
    	       <a href="/">Home</a>
    	   </p>
    	   <p>
    	       <a href="/contact">Contact</a>
    	   </p>    	  
    	</nav>
    	<div><p>Este é o segundo exemplo em CI</p></div>
    	<footer>
    	   <p>
    	       &copy; Copyright by Celso
    	   </p>    	  
    	</footer>
    </body>
</html>