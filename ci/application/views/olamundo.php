<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">
        <title>Olá Mundo CI</title>
    </head>
    <body>
    	<header>
    	   <h1>Olá Mundo CI</h1>
    	</header>
    	<nav>
    	   <p>
    	       <a href="/">Home</a>
    	   </p>
    	   <p>
    	       <a href="/contact">Contact</a>
    	   </p>    	  
    	</nav>
    	<div><p>Este é meu primeiro exemplo em CI</p></div>
    	<footer>
    	   <p>
    	       &copy; Copyright by Celso
    	   </p>    	  
    	</footer>
    </body>
</html>