<?php
echo '<h2>Lista de usuários</h2>';
if($this->session->flashdata('excluirok')):
echo '<p>', $this->session->flashdata('excluirok'), '</p>';
endif;
echo $this->table->set_heading('ID','Nome','Email','Login','Operações');
foreach($usuarios as $row):
    $this->table->add_row($row->id,$row->nome,$row->email,$row->login,anchor("crud/update/$row->id",'Editar').' - '.anchor("crud/delete/$row->id",'Excluir'));
endforeach;
echo $this->table->generate();