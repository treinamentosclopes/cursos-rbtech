<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">
        <title>Exemplo 3</title>
    </head>
    <body>
    	<header>
    	   <h1><?php echo (isset($titulo)) ? $titulo : 'Titulo vazio'; ?></h1>
    	</header>
    	<nav>
    	   <ul>
    	       <?php foreach($menu as $item): ?>
    	           <li><?php echo $item; ?></li>
    	       <?php endforeach; ?>
    	   </ul>    	  
    	</nav>
    	<div><p><?php echo $texto; ?></p>
    	   <p><?php echo $segmento; ?></p>
    	</div>
    	<footer>
    	   <p>
    	       &copy; Copyright by Celso
    	   </p>    	  
    	</footer>
    </body>
</html>